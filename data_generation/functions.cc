#include "proto.h"
#include "func_header.h"

void flux_check (int* k, int (*neighbour) (int, int, int)) {
	int x = 0;
	int previous_x;
	int flux = 0;
	int flaws = 0;
	while (x < nsites) {
		for (int d = 0; d < dim; d++) {
			previous_x = neighbour(x, d, -1);
			flux += k[d*nsites + x] - k[d*nsites + previous_x];
		}
		if (flux != 0) {
			cout << "Flux not conserved at " << x << endl;
			flaws++;
		}
		flux = 0;
		x++;
	}
	if (flaws == 0) cout << "Flux conserved at every lattice site" << endl;
}

void f_check (int* f, int* k, int* l, int (*neighbour)(int, int, int)) {
	int x = 0;
	int previous_x;
	int flux = 0;
	int flaws = 0;
	while (x < nsites) {
		for (int d = 0; d < dim; d++) {
			previous_x = neighbour(x, d, -1);
			flux += abs(k[d*nsites + x]) + abs(k[d*nsites + previous_x]) + 2 * (l[d*nsites + x] + l[d*nsites + previous_x]);
		}
		if (flux != f[x]) {
			cout << "Mismatch at " << x << endl;
			flaws++;
		}
		flux = 0;
		x++;
	}
	if (flaws == 0) cout << "Match at every lattice site" << endl;
}

int neighbour (int head, int dir, int shift) {
	int new_head;
	switch (dir) {
		case 0 : {
			new_head = head + (head + nt + shift) % nt - head % nt;
			break;
			}
		case 1 : {
			new_head = head + (head + nt * nx + shift * nt) % (nt * nx) - head % (nt * nx);
			break;
			}
		#if dim > 2
		case 2 : {
			new_head = head + (head + nt * nx * ny + shift * nt * nx) % (nt * nx * ny) - head % (nt * nx * ny);
			break;
			}
		#if dim > 3
		case 3 : {
			new_head = head + (head + nt * nx * ny * nz + shift * nt * nx * ny) % (nt * nx * ny * nz) - head % (nt * nx * ny * nz);
			break;
			}
		#endif
		#endif
		default : {
			break;
		}
	}
	
	return new_head;
}

void shuffler (MTRand& rgen, int* sequence) {
	int m, z;
	for (int n = 0; n < nlinks; n++) {
		m = rgen.randInt(nlinks - 1);
		z = sequence[n];
		sequence[n] = sequence[m];
		sequence[m] = z;
	}
}

void initialize (int* link_sequence, int* k, int* l, int* f, double* obs, int& j) {
	for (int n = 0; n < nlinks; n++) {
			k[n] = 0;
			l[n] = 0;
			link_sequence[n] = n;
		}
	for (int n = 0; n < nsites; n++) {
		f[n] = 0;
	}
	
	obs[0] = 0;
	obs[1] = 0;
	
	j = -thermal_skip;
}

void l_sweep (MTRand& rgen, int* link_sequence, int* k, int* l, int* f, double* W_ratio, double* inv_W_ratio) {
	for (int n = 0; n < nlinks; n++) {
		int link, site, dir, change, next_site, p, q;
		double prob;
		
		link = link_sequence[n];
		site = link % nsites;
		dir = link / nsites;
		change = rgen.randInt(1) * 2 - 1;
		next_site = neighbour(site, dir, 1);
		
		if (change == 1) {
			p = f[site] / 2;
			q = f[next_site] / 2;
			prob = W_ratio[p] * W_ratio[q] / (double) ((abs(k[link]) + l[link] + 1) * (l[link] + 1));
		}
		else {
			if (l[link] == 0) prob = 0;    //To avoid negative slots of the array
			else {
				p = (f[site] - 2) / 2;
				q = (f[next_site] - 2) / 2;
				prob = (double) (l[link] * (l[link] + abs(k[link])) * inv_W_ratio[p] * inv_W_ratio[q]);
			}
		}
		if (prob > rgen.randExc()) {
			l[link] += change;
			f[site] += 2 * change;
			f[next_site] += 2 * change;
			if (f[site] > f_max || f[next_site] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
		}
	}
}

//function that creates closed worms
void worm (MTRand& rgen, int (*neighbour)(int, int, int), int* k, int* l, int* f, double* weight, double* inv_weight, double mu) {
    int head, tail, dir, sign, delta, new_head, link, k0, k1, df, p, q;
    #if PHYSICAL_WEIGHTS
    int dft, r;
    #endif
    double prob;

    //Starting the worm
    bool started = false;

    while (!started) {
        head = rgen.randInt(nsites - 1);
        tail = head;
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        delta = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);
		
		#if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;
		
        if (k1 > k0) prob = inv_weight[q] / (double) (k1 + l[link]);
        else prob = inv_weight[q] * (double) (k0 + l[link]);
		
        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];
		
		prob *= inv_weight[p]
        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            dft = df;
            #else
            f[head] += df;
            f[new_head] += df;
            #endif
            head = new_head;
            started = true;
        }
    }

    //Running the worm
    do {
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);

        #if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;

        if (k1 > k0) {
            p = (f[head] + df + 1) / 2;
            prob = weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        }
        else {
            p = (f[head] + df - 1) / 2;
            prob = weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        }
		
        if (new_head == tail) {
            r = (f[new_head] + dft + k1 - k0) / 2;
            prob *= weight[r];
        }

        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];

        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            f[head] += df + k1 - k0;
            if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #else
            f[head] += df;
            f[new_head] += df;
            if (f[head] > f_max || f[new_head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #endif
            head = new_head;
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            #endif
        }
    }
    while (head != tail);
	
	#if PHYSICAL_WEIGHTS
	f[head] += dft + df;
	if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
	#endif
	#if FLUX_CONSERVATION_CHECK
	flux_check(k, neighbour);
	#endif
	#if F_CHECK
	f_check(f, k, l, neighbour);
	#endif
}

//function that creates closed worms and prints out their length and corresponding Monte Carlo steps
void worm (MTRand& rgen, int (*neighbour)(int, int, int), int* k, int* l, int* f, double* weight, double* inv_weight, double mu,
		   ofstream& file) {
    int head, tail, dir, sign, delta, new_head, link, k0, k1, df, p, q;
	int mc_steps = 0, length = 0;
    #if PHYSICAL_WEIGHTS
    int dft, r;
    #endif
    double prob;

    //Starting the worm
    bool started = false;

    while (!started) {
        head = rgen.randInt(nsites - 1);
        tail = head;
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        delta = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);
		
		#if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;
		
        if (k1 > k0) prob = inv_weight[q] / (double) (k1 + l[link]);
        else prob = inv_weight[q] * (double) (k0 + l[link]);
		
        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];
		
        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            dft = df;
            #else
            f[head] += df;
            f[new_head] += df;
            #endif
            head = new_head;
			length++;
            started = true;
        }
		
		mc_steps++;
    }

    //Running the worm
    do {
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;
        

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);

        #if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;

        if (k1 > k0) {
            p = (f[head] + df + 1) / 2;
            prob = weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        }
        else {
            p = (f[head] + df - 1) / 2;
            prob = weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        }
		
        if (new_head == tail) {
            r = (f[new_head] + dft + k1 - k0) / 2;
            prob *= weight[r];
        }

        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];

        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            f[head] += df + k1 - k0;
            if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #else
            f[head] += df;
            f[new_head] += df;
            if (f[head] > f_max || f[new_head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #endif
            head = new_head;
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            #endif
			length++;
        }
		
		mc_steps++;
    }
    while (head != tail);
	
	#if PHYSICAL_WEIGHTS
	f[head] += dft + df;
	if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
	#endif
	#if FLUX_CONSERVATION_CHECK
	flux_check(k, neighbour);
	#endif
	#if F_CHECK
	f_check(f, k, l, neighbour);
	#endif
	
	file << mc_steps << " " << length << "\n";
}

//function that creates closed worms and prints out open worms
void worm (MTRand& rgen, int (*neighbour)(int, int, int), int* k, int* l, int* f, double* weight, double* inv_weight, double mu,
		   bool* traffic_light) {
    int head, tail, dir, sign, delta, new_head, link, k0, k1, df, p, q, checkpoint_head;
	int length = 0;
	int k_checkpoint[nlinks];
    #if PHYSICAL_WEIGHTS
    int dft, r;
    #endif
    double prob;

    //Starting the worm
    bool started = false;

    while (!started) {
        head = rgen.randInt(nsites - 1);
		if (traffic_light[head]) continue;
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);
		if (traffic_light[new_head]) continue;
        tail = head;
        delta = rgen.randInt(1) * 2 - 1;

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);
		
		#if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;
		
        if (k1 > k0) prob = inv_weight[q] / (double) (k1 + l[link]);
        else prob = inv_weight[q] * (double) (k0 + l[link]);
		
        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];
		
		prob *= inv_weight[p]
        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            dft = df;
            #else
            f[head] += df;
            f[new_head] += df;
            #endif
            head = new_head;
			length++;
            started = true;
        }
    }

    //Running the worm
    do {
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);
		if (traffic_light[new_head]) continue;

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);

        #if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;

        if (k1 > k0) {
            p = (f[head] + df + 1) / 2;
            prob = weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        }
        else {
            p = (f[head] + df - 1) / 2;
            prob = weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        }
		
        if (new_head == tail) {
            r = (f[new_head] + dft + k1 - k0) / 2;
            prob *= weight[r];
        }

        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];

        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
			if (rgen.randExc() * length < 1) {
                for (int i = 0; i < nlinks; i++) k_checkpoint[i] = k[i];
                checkpoint_head = head;
			}
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            f[head] += df + k1 - k0;
            if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #else
            f[head] += df;
            f[new_head] += df;
            if (f[head] > f_max || f[new_head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #endif
            head = new_head;
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            #endif
			length++;
        }
    }
    while (head != tail);
	
	#if PHYSICAL_WEIGHTS
	f[head] += dft + df;
	if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
	#endif
	#if FLUX_CONSERVATION_CHECK
	flux_check(k, neighbour);
	#endif
	#if F_CHECK
	f_check(f, k, l, neighbour);
	#endif
	
	for (int i = 0; i < nlinks; i++) k[i] = k_checkpoint[i];
	traffic_light[tail] = true;
	traffic_light[checkpoint_head] = true;
    for (int x = 0; x < nsites; x++) {
        f[x] = 0;
		for (int d = 0; d < dim; d++) {
			int previous_x = neighbour(x, d, -1);
			f[x] += abs(k[d*nsites + x]) + abs(k[d*nsites + previous_x]) + 2 * (l[d*nsites + x] + l[d*nsites + previous_x]);
		}
    }
}


//function that creates closed worms and prints out open worms with their properties
void worm (MTRand& rgen, int (*neighbour)(int, int, int), int* k, int* l, int* f, double* weight, double* inv_weight, double mu,
		   bool* traffic_light, ofstream& file) {
    int head, tail, dir, sign, delta, new_head, link, k0, k1, df, p, q, checkpoint_head, checkpoint_mc_steps, checkpoint_length;
	int mc_steps = 0, length = 0;
	int k_checkpoint[nlinks];
    #if PHYSICAL_WEIGHTS
    int dft, r;
    #endif
    double prob;

    //Starting the worm
    bool started = false;

    while (!started) {
        head = rgen.randInt(nsites - 1);
		if (traffic_light[head]) continue;
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);
		if (traffic_light[new_head]) continue;
        tail = head;
        delta = rgen.randInt(1) * 2 - 1;

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);
		
		#if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;
		
        if (k1 > k0) prob = inv_weight[q] / (double) (k1 + l[link]);
        else prob = inv_weight[q] * (double) (k0 + l[link]);
		
        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];
		
		prob *= inv_weight[p]
        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            dft = df;
            #else
            f[head] += df;
            f[new_head] += df;
            #endif
            head = new_head;
			length++;
            started = true;
        }
		
		mc_steps++;
    }

    //Running the worm
    do {
        dir = rgen.randInt(dim - 1);
        sign = rgen.randInt(1) * 2 - 1;
        new_head = neighbour(head, dir, sign);
		if (traffic_light[new_head]) continue;

        if (sign == 1) link = dir * nsites + head;
        else link = dir * nsites + new_head;

        k0 = abs(k[link]);
        k1 = abs(k[link] + delta * sign);

        #if PHYSICAL_WEIGHTS
        q = f[new_head] / 2;

        if (k1 > k0) {
            p = (f[head] + df + 1) / 2;
            prob = weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        }
        else {
            p = (f[head] + df - 1) / 2;
            prob = weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        }
		
        if (new_head == tail) {
            r = (f[new_head] + dft + k1 - k0) / 2;
            prob *= weight[r];
        }

        #else
        df = k1 - k0;
        p = f[head];
        q = f[new_head];

        if (df > 0) prob = weight[p + 1] * weight[q + 1] * inv_weight[p] * inv_weight[q] / (double) (k1 + l[link]);
        else prob = weight[p - 1] * weight[q - 1] * inv_weight[p] * inv_weight[q] * (double) (k0 + l[link]);
        #endif

        if (dir == 0) prob *= exp(mu * delta * sign);

        if (prob > rgen.randExc()) {
			if (rgen.randExc() * length < 1) {
                for (int i = 0; i < nlinks; i++) k_checkpoint[i] = k[i];
                checkpoint_head = head;
                checkpoint_mc_steps = mc_steps;
                checkpoint_length = length;
			}
            k[link] += delta * sign;
            /*#if FLUX_CONSERVATION_CHECK			//To check that until the worm closes, the constraint is violated
            flux_check(k, neighbour);
            #endif*/
            #if PHYSICAL_WEIGHTS
            f[head] += df + k1 - k0;
            if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #else
            f[head] += df;
            f[new_head] += df;
            if (f[head] > f_max || f[new_head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
            #endif
            head = new_head;
            #if PHYSICAL_WEIGHTS
            df = k1 - k0;
            #endif
			length++;
        }
		
		mc_steps++;
    }
    while (head != tail);
	
	#if PHYSICAL_WEIGHTS
	f[head] += dft + df;
	if (f[head] > f_max) cout << "f is too large! Increase its threshold under the command line argument f_max\n";
	#endif
	#if FLUX_CONSERVATION_CHECK
	flux_check(k, neighbour);
	#endif
	#if F_CHECK
	f_check(f, k, l, neighbour);
	#endif
	
	for (int i = 0; i < nlinks; i++) k[i] = k_checkpoint[i];
	traffic_light[tail] = true;
	traffic_light[checkpoint_head] = true;
    for (int x = 0; x < nsites; x++) {
        f[x] = 0;
		for (int d = 0; d < dim; d++) {
			int previous_x = neighbour(x, d, -1);
			f[x] += abs(k[d*nsites + x]) + abs(k[d*nsites + previous_x]) + 2 * (l[d*nsites + x] + l[d*nsites + previous_x]);
		}
    }
	file << checkpoint_mc_steps << " " << checkpoint_length << " " << tail << " " << checkpoint_head << "\n";
}

void obs_update (int* k, int* f, double* W_ratio, double* obs) {
	int k0_sum = 0;
	double W_sum = 0.;
	
	for (int l = 0; l < nsites; l++) {
		k0_sum += k[l];
		int p = f[l] / 2;
		W_sum += W_ratio[p];
	}
	
	obs[0] = k0_sum / (double) nsites;
	obs[1] = W_sum / (double) nsites;
}

void write_config (int* k, int* l, ofstream& file) {
	for (int n = 0; n < nlinks; n++) file << k[n] << " ";
	file << "\n";
	for (int n = 0; n < nlinks; n++) file << l[n] << " ";
	file << "\n";
}

void file_creator (string& filename, ofstream& file) {
    //Creating filename
    char dims[16];
    sprintf(dims, "-%d-%d", nt, nx);
    string Dims = string(dims);
    filename += Dims;
	
    #ifdef ny
    char ydim[8];
    sprintf(ydim, "-%d", ny);
    string Ydim = string(ydim);
    filename += Ydim;
    #endif
	
    #ifdef nz
    char zdim[8];
    sprintf(zdim, "-%d", nz);
    string Zdim = string(zdim);
    filename += Zdim;
    #endif
	
    char numbers[32];
    sprintf(numbers, "-%.3f-%.3f-%.3f", eta, lam, mu);
    string Numbers = string(numbers);
    filename += Numbers;
	
    char extension[4];
    sprintf(extension, ".dat");
    string Extension = string(extension);
    filename += Extension;
	
    //Opening file
    file.open(filename, ios::out);
}

void open_worms_file_creator (string& filename, ofstream& file, int open_worms) {
    //Creating filename
    char worm_number[4];
    sprintf(worm_number, "-%d", open_worms);
    string Worm_number = string(worm_number);
    filename += Worm_number;
    
    char dims[16];
    sprintf(dims, "-%d-%d", nt, nx);
    string Dims = string(dims);
    filename += Dims;
	
    #ifdef ny
    char ydim[8];
    sprintf(ydim, "-%d", ny);
    string Ydim = string(ydim);
    filename += Ydim;
    #endif
	
    #ifdef nz
    char zdim[8];
    sprintf(zdim, "-%d", nz);
    string Zdim = string(zdim);
    filename += Zdim;
    #endif
	
    char numbers[32];
    sprintf(numbers, "-%.3f-%.3f-%.3f", eta, lam, mu);
    string Numbers = string(numbers);
    filename += Numbers;
	
    char extension[4];
    sprintf(extension, ".dat");
    string Extension = string(extension);
    filename += Extension;
	
    //Opening file
    file.open(filename, ios::out);
}