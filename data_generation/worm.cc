#include <ctime>
#include "MersenneTwister.h"
#include "proto.h"
#include "W.h"
#include "header.h"

int main () {
	
	int k[nlinks];
	int l[nlinks];
	int link_sequence[nlinks];
	int f[nsites];
	double obs[2];
	
	MTRand rgen;
	rgen.seed(time(0));
	
	int iteration;
    int max_iter = (ndata - 1) * sweeps + delay;
	
	do {
        #if OBS
        ofstream n_meas, phi_meas;
        string n_name = "n";
		string phi_name = "phi2";
		file_creator(n_name, n_meas);
		file_creator(phi_name, phi_meas);
		n_meas << setiosflags(ios::scientific);
		phi_meas << setiosflags(ios::scientific);
        #endif
		#if CONFIG
		ofstream config;
		string conf_name = "configs";
		file_creator(conf_name, config);
        #endif
		#if OPEN_CONFIG
		ofstream open_config;
		string open_conf_name = "open_configs";
		open_worms_file_creator(open_conf_name, open_config, open_worms);
        #endif
        #if WORM_PROPERTIES
        ofstream wp;
		string wp_name = "worms";
		file_creator(wp_name, wp);
        #endif
		#if OPEN_WORM_PROPERTIES
        ofstream owp;
		string owp_name = "open_worms";
		open_worms_file_creator(owp_name, owp, open_worms);
        #endif
		
		initialize(link_sequence, k, l, f, obs, iteration);
		
		do {
			shuffler(rgen, link_sequence);			//To avoid any possible bias related to considering always the same link sequence
			l_sweep(rgen, link_sequence, k, l, f, W_ratio, inv_W_ratio);
            
            #if WORM_PROPERTIES
			if (iteration % sweeps == 0 && iteration >= 0) worm(rgen, neighbour, k, l, f, W, inv_W, mu, wp);
			else worm(rgen, neighbour, k, l, f, W, inv_W, mu);
			#else
			worm(rgen, neighbour, k, l, f, W, inv_W, mu);
			#endif
			
			
			#if OPEN_CONFIG || OPEN_WORM_PROPERTIES
			if ((iteration - delay) % sweeps == 0 && iteration >= delay) {
				int k_copy[nlinks];
				int f_copy[nsites];
				bool traffic_light[nsites];
				for (int i = 0; i < nlinks; i++) k_copy[i] = k[i];
				for (int i = 0; i < nsites; i++) {
					traffic_light[i] = false;
					f_copy[i] = f[i];
				}
				for (int n = 0; n < open_worms; n++) {
					#if OPEN_WORM_PROPERTIES
					worm (rgen, neighbour, k_copy, l, f_copy, W, inv_W, mu, traffic_light, owp);
					#else
					worm (rgen, neighbour, k_copy, l, f_copy, W, inv_W, mu, traffic_light);
					#endif
				}
				#if OPEN_CONFIG
				write_config(k_copy, l, open_config);
				#endif
			}
			#endif
			
			#if OBS || CONFIG
			if (iteration % sweeps == 0 && iteration >= 0) {
				#if OBS
				obs_update(k, f, W_ratio, obs);
				n_meas << setprecision(10) << obs[0] << "\n";
				phi_meas << setprecision(10) << obs[1] << "\n";
				#endif
				
				#if CONFIG
				write_config(k, l, config);
				#endif
			}
			#endif
			
			iteration++;
		}
		while (iteration <= max_iter);
		
		#if OBS
        n_meas.close();
        phi_meas.close();
        #endif
        #if CONFIG
        config.close();
        #endif
		#if OPEN_CONFIG
        open_config.close();
        #endif
		#if WORM_PROPERTIES
		wp.close();
		#endif
		#if OPEN_WORM_PROPERTIES
		owp.close();
		#endif
        
		cout << "mu = " << mu << ": simulation completed.\n";
		
		mu += mu_step;
	}
	while(mu < mu_max);
	
	return 0;
}