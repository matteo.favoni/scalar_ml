#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <string>
#include "MersenneTwister.h"
using namespace std;

void flux_check(int*, int (*)(int, int, int));
void f_check(int*, int*, int*, int (*)(int, int, int));
int neighbour (int, int, int);
void shuffler (MTRand& rgen, int*);
void initialize (int*, int*, int*, int*, double*, int&);
void l_sweep (MTRand& rgen, int*, int*, int*, int*, double*, double*);
void worm (MTRand& rgen, int (*)(int, int, int), int*, int*, int*, double*, double*, double);
void worm (MTRand& rgen, int (*)(int, int, int), int*, int*, int*, double*, double*, double, ofstream&);
void worm (MTRand& rgen, int (*)(int, int, int), int*, int*, int*, double*, double*, double, bool*);
void worm (MTRand& rgen, int (*)(int, int, int), int*, int*, int*, double*, double*, double, bool*, ofstream&);
void obs_update (int*, int*, double*, double*);
void write_config (int*, int*, ofstream&);
void file_creator (string&, ofstream&);
void open_worms_file_creator (string&, ofstream&, int);