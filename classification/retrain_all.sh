# This script re-trains the best architectures found by optuna.

# equivariant architectures
python retrain_cmd_hparams.py "eq_arch" "equivariant" 1000 500 50 1000 "{'name': 'eq_search_2_4000_train_samples', 'num_workers': 0, 'lr': 0.001, 'batch_size': 100, 'weight_decay': 0.0, 'lr_min': 0.001, 'period_epochs': 10, 'biases': [True, True], 'kernels': [[2, 2], [1, 1]], 'channels': [32, 32], 'eq_pools': [None, None], 'pooling_mode': 'maxpool', 'dense_biases': [True], 'dense_sizes': [32]}"

# strided architectures
python retrain_cmd_hparams.py "st_arch" "nonequivariant_gp" 1000 500 50 1000 "{'name': 'st_search_1_4000_train_samples', 'num_workers': 0, 'lr': 0.001, 'batch_size': 100, 'weight_decay': 0.0, 'lr_min': 0.001, 'period_epochs': 10, 'biases': [False, True, True], 'kernels': [[2, 2], [1, 1], [1, 1]], 'channels': [16, 16, 8], 'strided_pools': ['maxpool', None, None], 'global_pooling_mode': 'maxpool', 'dense_biases': [True], 'dense_sizes': [32]}"

# flatenning architectures
python retrain_cmd_hparams.py "flat_arch" "nonequivariant_fl" 1000 500 50 1000 "{'name': 'flat_search_1_4000_train_samples', 'num_workers': 0, 'lr': 0.001, 'batch_size': 100, 'weight_decay': 0.0, 'lr_min': 0.001, 'period_epochs': 10, 'biases': [False, True, True], 'kernels': [[3, 3], [2, 2], [2, 2]], 'channels': [8, 32, 32], 'strided_pools': ['maxpool', 'avgpool', None], 'dense_biases': [], 'dense_sizes': [], 'nt': 8, 'nx': 8}"

