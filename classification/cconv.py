import torch

def update_kernel_size(kernel_size):
    new_kernel_size = kernel_size
    if type(kernel_size) == int:
        new_kernel_size = [kernel_size, kernel_size]
    elif type(kernel_size) == list:
        if len(kernel_size) == 1:
            new_kernel_size = [kernel_size[0], kernel_size[0]]
    return new_kernel_size
            

class CConv2d(torch.nn.Module):
    """
        A wrapper for Conv2d which correctly implements circular padding.
    """
    def __init__(self, in_channels, out_channels, kernel_size, bias=True):
        super(CConv2d, self).__init__()
        
        # Conv2d without padding
        self.conv = torch.nn.Conv2d(in_channels, out_channels, kernel_size, bias=bias)
        
        # padding size
        self.padding = []
        
        # Check and fix size of kernel_size
        kernel_size = update_kernel_size(kernel_size)
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel)
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_conv = self.conv(x_pad)
        return x_conv


class CMaxPool2d(torch.nn.Module):
    """
        A wrapper for MaxPool2d which correctly implements circular padding.
    """
    def __init__(self, kernel_size, stride):
        super(CMaxPool2d, self).__init__()
        
        # MaxPool2d without padding
        self.pool = torch.nn.MaxPool2d(kernel_size, stride)
        
        # padding size
        self.padding = []
        
        # Check and fix size of kernel_size
        kernel_size = update_kernel_size(kernel_size)
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel)
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_pool = self.pool(x_pad)
        return x_pool


class CAvgPool2d(torch.nn.Module):
    """
        A wrapper for AvgPool2d which correctly implements circular padding.
    """
    def __init__(self, kernel_size, stride):
        super(CAvgPool2d, self).__init__()
        
        # AvgPool2d without padding
        self.pool = torch.nn.AvgPool2d(kernel_size, stride)
        
        # padding size
        self.padding = []
        
        # Check and fix size of kernel_size
        kernel_size = update_kernel_size(kernel_size)
        
        # for some reason `padding` is in reverse order compared to kernel_size (see also pytorch repository)
        for k in reversed(kernel_size):
            if k % 2 == 0:
                # even kernel
                self.padding.append((k - 1) // 2)
                self.padding.append(k // 2)
            else:
                # odd kernel)
                self.padding.append(k // 2)
                self.padding.append(k // 2)
    
    def forward(self, x):
        x_pad = torch.nn.functional.pad(x, self.padding, mode='circular')
        x_pool = self.pool(x_pad)
        return x_pool
