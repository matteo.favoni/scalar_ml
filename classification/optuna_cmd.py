"""
    This script uses optuna to search for well-performing architectures of a
    particular type (EQ, ST, FLAT) in a given search space. This script has been
    adapted for the classification task.
    
    Optuna selects trial architectures from the search space which are then 
    trained and validated. This is repeated a few times to avoid lucky or
    unlucky random initializations. Optuna searches for an architecture with
    the smallest average validation loss.
    
    Usage:
    train_optuna.py run_name model_type num_train num_val_test num_repeats max_epochs
    - run_name: defines the output filenames of the run
    - model_type: either 'equivariant' (EQ), 'nonequivariant_gp' (ST) or
         'nonequivariant_fl' (FLAT)
    - num_train: number of training samples to use (will be multiplied by 4,
        2 training points, 2 classes)
    - num_val_test: validation and test samples (will be multiplied by 4)
    - num_repeats: number of random initializations per trial
    - max_epochs: if early stopping does not stop training beforehand, only
        train up to 'max_epochs'
"""
    
    
"""
    Import packages
"""

import torch
import numpy as np
import os
import pytorch_lightning as pl
import argparse
import pickle
from tqdm import tqdm
import optuna
from operator import itemgetter
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
import inspect
import datapaths

# suppress warning and logging
import warnings
import logging
import sys
warnings.filterwarnings('ignore')
logging.disable(sys.maxsize)

"""
    Command-line options
"""

parser = argparse.ArgumentParser()

parser.add_argument("run_name", help="name for optuna run", type=str)
parser.add_argument("model_type", help="type of model", type=str, choices=['equivariant', 'nonequivariant_gp', 'nonequivariant_fl'])

parser.add_argument("num_train", help="number of training examples", type=int, default=1000)
parser.add_argument("num_val_test", help="number of validation and test examples", type=int, default=500)
parser.add_argument("num_trials", help="number of optuna trials", type=int,  default=400)
parser.add_argument("num_repeats", help="number of repeats per trial", type=int,  default=5)
parser.add_argument("max_epochs", help="max. number of epochs to train", type=int,  default=200)

args = parser.parse_args()

"""
    Options
"""

# path to datasets
path_data = datapaths.path_data
path_root = os.getcwd()

# run name
run_type = args.model_type
run_basename = args.run_name

# number of unique trials per optuna run
num_trials = args.num_trials

# number of training examples
num_train_samples = list([args.num_train])
num_train_samples = np.unique(num_train_samples)
train_sample_factor = 4

# set validation and test indices, so there is no overlap of data
num_val_and_test = args.num_val_test
validation_index_start = num_train_samples[-1]
validation_index_end = validation_index_start + num_val_and_test
test_index_start = validation_index_end
test_index_end = validation_index_end + num_val_and_test

# training parameters (lattice size, physical parameters)
# all lists should be of same length
mus = [1.000, 1.000, 1.500, 1.500]
worm_nums = [0, 1, 0, 1]
dims = [(8, 8), (8, 8), (8, 8), (8, 8)]
ms = [4.250, 4.250, 4.010, 4.010]
gs = [1.000, 1.000, 1.000, 1.000]

# maximum number of epochs to train for
max_epochs = args.max_epochs

# whether to print number of parameters before training
print_num_params = True

# repeats per trial
num_repeats = args.num_repeats

"""
    Definitions for each type of model including search spaces
"""

# EQUIVARIANT (EQ) ARCHITECTURE
if run_type == 'equivariant':
    # import model code
    from models import WormClassifier as Model
    
    # hparams proposal function for optuna
    def propose_hparams(trial, hparams_defaults):
        # make a copy of default hparams
        hparams = argparse.Namespace(**vars(hparams_defaults))

        # suggest architecture
        max_convs = 3
        max_kernel_size = 3
        choices_conv_channels = [4, 8, 16, 32]
        max_dense = 2
        choices_dense_size = [4, 8, 16, 32]

        # convolutional part
        num_layers_nxn = trial.suggest_int('num_nxn_convs', 1, max_convs)
        biases = []
        kernels = []
        channels = []
        for i in range(num_layers_nxn):
            layer_name = 'conv_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            kernel_size = trial.suggest_int(layer_name + '_kernel', 1, max_kernel_size)
            kernel_size = [kernel_size, kernel_size]
            num_channels = trial.suggest_categorical(layer_name + '_channels', choices_conv_channels)

            biases.append(bias)
            kernels.append(kernel_size)
            channels.append(num_channels)
        
        # allow equivariant pool (stride=1) after every conv, except the last
        num_eq_pools = 0
        eq_pools = [None] * num_layers_nxn
        for i in range(num_layers_nxn - 1):
            layer_name = 'eq_pool_{:d}'.format(i)
            eq_pool_type = trial.suggest_categorical(layer_name, [None, 'maxpool', 'avgpool'])
            eq_pools[i] = eq_pool_type

            if eq_pool_type is not None:
                num_eq_pools += 1
           
        hparams.biases = biases
        hparams.kernels = kernels
        hparams.channels = channels
        hparams.eq_pools = eq_pools

        # global pooling
        valid_pool_modes = ['maxpool']
        pooling_mode = trial.suggest_categorical('pool_mode', valid_pool_modes)
        hparams.pooling_mode = pooling_mode

        # linear (dense) layers
        num_layers_dense = trial.suggest_int('num_dense', 0, max_dense)
        dense_biases = []
        dense_sizes = []
        for i in range(num_layers_dense):
            layer_name = 'dense_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            size = trial.suggest_categorical(layer_name + '_size', choices_dense_size)
            dense_biases.append(bias)
            dense_sizes.append(size)
        hparams.dense_biases = dense_biases
        hparams.dense_sizes = dense_sizes

        return hparams

# STRIDED (ST) ARCHITECTURE
elif run_type == 'nonequivariant_gp':
    # import model code
    from models import WormClassifierNEQGP as Model
    
    # hparams proposal function for optuna
    def propose_hparams(trial, hparams_defaults):
        # make a copy of default hparams
        hparams = argparse.Namespace(**vars(hparams_defaults))

        # suggest architecture
        max_convs = 3 
        max_kernel_size = 3
        choices_conv_channels = [4, 8, 16, 32]
        max_dense = 2
        choices_dense_size = [4, 8, 16, 32]

        # convolutional part
        num_layers_nxn = trial.suggest_int('num_nxn_convs', 1, max_convs)
        biases = []
        kernels = []
        channels = []
        strided_pools = [None] * num_layers_nxn

        for i in range(num_layers_nxn):
            layer_name = 'conv_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            kernel_size = trial.suggest_int(layer_name + '_kernel', 1, max_kernel_size)
            kernel_size = [kernel_size, kernel_size]
            num_channels = trial.suggest_categorical(layer_name + '_channels', choices_conv_channels)

            biases.append(bias)
            kernels.append(kernel_size)
            channels.append(num_channels)

        # allow strided pool after every conv, except the last
        num_strided_pools = 0
        for i in range(num_layers_nxn - 1):
            layer_name = 'strided_pool_{:d}'.format(i)
            strided_pool_type = trial.suggest_categorical(layer_name, [None, 'maxpool', 'avgpool'])
            strided_pools[i] = strided_pool_type

            if strided_pool_type is not None:
                num_strided_pools += 1

        if num_strided_pools == 0:
            print("No strided pools. Skipping.")
            raise optuna.structs.TrialPruned('No strided pools.')

        hparams.biases = biases
        hparams.kernels = kernels
        hparams.channels = channels
        hparams.strided_pools = strided_pools

        # global pooling
        valid_pool_modes = ['maxpool']
        global_pooling_mode = trial.suggest_categorical('global_pool_mode', valid_pool_modes)
        hparams.global_pooling_mode = global_pooling_mode

        # linear layers
        num_layers_dense = trial.suggest_int('num_dense', 0, max_dense)
        dense_biases = []
        dense_sizes = []
        for i in range(num_layers_dense):
            layer_name = 'dense_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            size = trial.suggest_categorical(layer_name + '_size', choices_dense_size)
            dense_biases.append(bias)
            dense_sizes.append(size)
        hparams.dense_biases = dense_biases
        hparams.dense_sizes = dense_sizes

        return hparams

# FLATTENING (FLAT) ARCHITECTURE
elif run_type == 'nonequivariant_fl':
    # import model code
    from models import WormClassifierNEQF as Model
    
    # hparams proposal function for optuna
    def propose_hparams(trial, hparams_defaults):
        # make a copy of default hparams
        hparams = argparse.Namespace(**vars(hparams_defaults))
        
        # suggest architecture
        max_convs = 3 
        max_kernel_size = 3
        choices_conv_channels = [4, 8, 16, 32]
        max_dense = 2
        choices_dense_size = [4, 8, 16, 32]

        # convolutional part
        num_layers_nxn = trial.suggest_int('num_nxn_convs', 1, max_convs)
        biases = []
        kernels = []
        channels = []
        strided_pools = [None] * num_layers_nxn    

        for i in range(num_layers_nxn):
            layer_name = 'conv_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            kernel_size = trial.suggest_int(layer_name + '_kernel', 1, max_kernel_size)
            kernel_size = [kernel_size, kernel_size]
            num_channels = trial.suggest_categorical(layer_name + '_channels', choices_conv_channels)
            
            biases.append(bias)
            kernels.append(kernel_size)
            channels.append(num_channels)

        # allow strided pool after every conv, except the last
        num_strided_pools = 0
        for i in range(num_layers_nxn - 1):
            layer_name = 'strided_pool_{:d}'.format(i)
            strided_pool_type = trial.suggest_categorical(layer_name, [None, 'maxpool', 'avgpool'])
            strided_pools[i] = strided_pool_type

            if strided_pool_type is not None:
                num_strided_pools += 1

        hparams.biases = biases
        hparams.kernels = kernels
        hparams.channels = channels
        hparams.strided_pools = strided_pools

        # linear layers
        num_layers_dense = trial.suggest_int('num_dense', 0, max_dense)
        dense_biases = []
        dense_sizes = []
        for i in range(num_layers_dense):
            layer_name = 'dense_{:d}'.format(i)
            bias = trial.suggest_categorical(layer_name + '_bias', [False, True])
            size = trial.suggest_categorical(layer_name + '_size', choices_dense_size)
            dense_biases.append(bias)
            dense_sizes.append(size)
        hparams.dense_biases = dense_biases
        hparams.dense_sizes = dense_sizes

        # lattice size added for flattening models
        hparams.nt = dims[0][0]
        hparams.nx = dims[0][1]
        
        return hparams
    
else:
    print("Unknown run_type: {}".format(run_type))
    exit()

"""
    Load datasets
"""


def get_dataset_path(worm_num, dim, m, g, mu):
    nt, nx = dim
    # format: worm_num, nt, nx, m, g, mu
    dataset_string = "dataset-{:d}-{:d}-{:d}-{:.3f}-{:.3f}-{:.3f}.pt"
    dataset_path = os.path.join(path_data, dataset_string.format(worm_num, nt, nx, m, g, mu))
    if not os.path.exists(dataset_path):
        raise Exception("Path not found: " + dataset_path)
    return dataset_path


def concat_datasets(worm_nums, dims, ms, gs, mus, n_start=0, n_stop=-1):  
    # check if all lists have same length
    if not all([len(worm_nums) == len(lst) for lst in [dims, ms, gs, mus]]):
        print("List of parameters must all be same length!")
        raise
    
    paths = []
    for i in range(len(worm_nums)):
        paths.append(get_dataset_path(worm_nums[i], dims[i], ms[i], gs[i], mus[i]))
    
    datasets = []
    for p in paths:
        dataset = torch.load(p)

        if n_stop == -1:
            n_stop = len(dataset)            
        dataset = torch.utils.data.Subset(dataset, range(n_start, n_stop))
        
        datasets.append(dataset)
    
    combined_dataset = torch.utils.data.ConcatDataset(datasets)
        
    return combined_dataset


"""
    Optuna objective function
"""


class MetricsCallback(pl.Callback):
    def __init__(self):
        super().__init__()
        self.metrics = []
    
    def on_validation_end(self, trainer, pl_module):
        self.metrics.append(trainer.callback_metrics)


def objective(trial, hparams_defaults, train_data, val_data):
    print("##############")
    print("Starting trial {:d}".format(trial.number))
    
    # lists for averaging
    list_final_vloss = []
    list_final_vacc = []
    list_best_vloss = []
    
    # list of checkpoint paths
    list_checkpoint_paths = []
    
    for no_repeat in range(num_repeats):
        # make a copy of default hparams
        hparams = argparse.Namespace(**vars(hparams_defaults))
        
        # suggest architecture
        hparams = propose_hparams(trial, hparams_defaults)
        
        # Check if this combination of parameters was already tried and if so, do not try them again.
        for t in trial.study.trials:
            if t.state != optuna.structs.TrialState.COMPLETE:
                continue

            if t.params == trial.params:
                raise optuna.structs.TrialPruned('Duplicate parameter set')

        # initialize model with hparams
        # we wrap this in the with block to turn off pytorch lightning info
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            model = Model(hparams, train_data, val_data, val_data)

        # print number of parameters
        if print_num_params:
            print("Number of trainable parameters: {}".format(model.count_parameters()))

        # tensorboard logger
        log_name = hparams.name + '_trial_{:d}_{:d}'.format(trial.number, no_repeat)        
        tb = pl.loggers.TensorBoardLogger(save_dir=os.path.join(path_root, 'optuna_logs'), name=log_name)

        # checkpoints and metrics
        checkpoint_name = hparams.name + '_trial_{:d}_{:d}'.format(trial.number, no_repeat)
        checkpoint_callback = pl.callbacks.ModelCheckpoint(os.path.join(model_path, checkpoint_name, '{epoch:03d}-{val_loss:.2E}'),
                                                           monitor='val_loss')
        metrics_callback = MetricsCallback()

        early_stop_callback = EarlyStopping(monitor='val_loss', min_delta=0.00,
                                            patience=max_epochs // 4, verbose=True, mode='min')
                                            
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            trainer = pl.Trainer(gpus=1, min_epochs=max_epochs // 4, max_epochs=max_epochs, weights_summary=None,
                                 check_val_every_n_epoch=1,
                                 progress_bar_refresh_rate=5, logger=tb, callbacks=[metrics_callback, early_stop_callback],
                                 distributed_backend=None,
                                 num_sanity_val_steps=0, checkpoint_callback=checkpoint_callback)
            trainer.fit(model)
        
        final_vloss = metrics_callback.metrics[-1]['val_loss']
        final_vacc = model.vacc
        best_vloss = checkpoint_callback.best_model_score.item()
        
        # add stuff to lists
        list_final_vloss.append(final_vloss)
        list_final_vacc.append(final_vacc)
        list_best_vloss.append(best_vloss)
        list_checkpoint_paths.append(os.path.join(model_path, checkpoint_name))
    
    # add user attributes 
    trial.set_user_attr('vloss', np.mean(list_final_vloss))
    trial.set_user_attr('vloss_list', list_final_vloss)
    trial.set_user_attr('vacc', np.mean(list_final_vacc))
    trial.set_user_attr('vacc_list', list_final_vacc)
    trial.set_user_attr('best_vloss', np.mean(list_best_vloss))
    trial.set_user_attr('best_vloss_list', list_best_vloss)
    trial.set_user_attr('model_checkpoint_path', list_checkpoint_paths)
    trial.set_user_attr('hparams', hparams)

    print("***************")
    print("Checkpoint path:")
    print(list_checkpoint_paths)
    print("Hyperparamters:")
    print(hparams)
    print("vloss = {:.2E}, vacc = {:.3f}".format(np.mean(list_final_vloss), np.mean(list_final_vacc)))

    return np.mean(list_best_vloss)


"""
    Training code
"""

# redirect all print statements to tqdm.write
# store builtin print
old_print = print


def new_print(*args, **kwargs):
    # if tqdm.tqdm.write raises error, use builtin print
    try:
        tqdm.tqdm.write(*args, **kwargs)
    except:
        old_print(*args, ** kwargs)


# globaly replace print with new_print
inspect.builtins.print = new_print

# create optuna path
optuna_path = os.path.join(path_root, 'optuna')
if not os.path.isdir(optuna_path):
    os.mkdir(optuna_path)

with tqdm(total=len(num_train_samples)) as pbar_samples:
    for current_num_train_samples in num_train_samples:
        # create training and validation subsets
        train_data = concat_datasets(worm_nums, dims, ms, gs, mus, 0, current_num_train_samples)
        val_data = concat_datasets(worm_nums, dims, ms, gs, mus, validation_index_start, validation_index_end)
        
        run_name = run_basename + '_{}_train_samples'.format(train_sample_factor * current_num_train_samples)
        model_path = os.path.join(optuna_path, run_name)
        pickle_path = os.path.join(path_root, 'optuna_pickles')
        
        """
            Default hparams
        """
        hparams_defaults = argparse.Namespace()
        
        # name
        hparams_defaults.name = run_name
        
        # workers for dataloaders
        hparams_defaults.num_workers = 0
        
        # optimizer
        hparams_defaults.lr = 1e-3
        hparams_defaults.batch_size = 100
        hparams_defaults.weight_decay = 0.0
        
        # lr scheduling
        hparams_defaults.lr_min = hparams_defaults.lr # this turns off the scheduler
        hparams_defaults.period_epochs = 10
        
        # create study and fill with attributes
        study = optuna.create_study(direction='minimize')
        
        # optimize study
        with tqdm(total=num_trials) as pbar_unique_trials:
            # Try UNIQUE_TRIALS unique combinations of parameters, not total combinations.
            while num_trials > len(set(str(t.params) for t in study.trials)):
                study.optimize(lambda trial: objective(trial, hparams_defaults, train_data, val_data), n_trials=1)
                if study.trials[-1].value is not None:
                    pbar_unique_trials.update(1)
                    
                study_filename = run_name + '_study.pickle'

                study_path = os.path.join(pickle_path, study_filename)
                try:
                    if not os.path.isdir(pickle_path):
                        os.mkdir(pickle_path)
                except OSError:
                    print("Failed to create directory: {}".format(pickle_path))

                with open(study_filename, 'wb') as file:
                    pickle.dump(study, file)
        
        # Give an overview over the study and the best trial.
        print('Number of training data:', train_sample_factor * current_num_train_samples)
        print('Number of finished trials:', len(study.trials))
        print('Best trial:')
        trial = study.best_trial
        print('vloss:', trial.value)
        print('Params:')
        for key, value in trial.params.items():
            print('{}: {}'.format(key, value))
        print('\n')
        
        # Since we implemented the usage of unique trials by pruning trials for which the combination of parameters
        # had already been used before, the pruned trials show up with None as a value for the mean training loss.
        # We delete the corresponding trials before sorting by the mean loss and saving the results.
        model_results = [[study.trials[i].value, study.trials[i].params] for i in range(len(study.trials))]

        for i in range(len(model_results)-1, -1, -1):
            if model_results[i][0] is None:
                del model_results[i]

        model_results.sort(key=itemgetter(0))

        if not os.path.isdir(pickle_path):
            os.mkdir(pickle_path)
        
        filename = run_name + '.pickle'
        study_filename = run_name + '_study.pickle'
            
        with open(os.path.join(pickle_path, filename), 'wb') as file:
            pickle.dump(model_results, file)
        
        with open(os.path.join(pickle_path, study_filename), 'wb') as file:
            pickle.dump(study, file)
        
        pbar_samples.update(1)
        
print("All done!")
