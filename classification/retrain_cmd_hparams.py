"""
    This script re-trains a given architecture (specified by 'hparams' passed as a string) and validates
    the created ensemble.

    Usage:
    retrain_cmd_hparams.py run_name model_type num_train num_val_test num_models max_epochs
    - run_name: defines the output filenames of the run
    - model_type: either 'equivariant' (EQ), 'nonequivariant_gp' (ST) or 'nonequivariant_fl' (FLAT)
    - num_train: number of training samples to use (will be multiplied by 4, 2 training points, 2 classes)
    - num_val_test: validation and test samples (will be multiplied by 4)
    - num_models: number of models to train based on the architecture
    - max_epochs: if early stopping does not stop training beforehand, only train up to 'max_epochs'
"""

"""
    Import packages
"""

import torch
import numpy as np
import os
import pickle
from itertools import product
import pytorch_lightning as pl
import argparse
import pickle
import re
import pandas
from tqdm import tqdm
import optuna
from operator import itemgetter
import itertools
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
import json
import datapaths
import inspect

# suppress warning and logging
import warnings
import logging
import sys
warnings.filterwarnings('ignore')
logging.disable(sys.maxsize)

"""
    Command-line options
"""

parser = argparse.ArgumentParser()

parser.add_argument("run_name", help="name for optuna run", type=str)
parser.add_argument("model_type", help="type of model", type=str, choices=['equivariant', 'nonequivariant_gp', 'nonequivariant_fl'])
parser.add_argument("num_train", help="number of training examples (x4)", type=int, default=1000)
parser.add_argument("num_val_test", help="number of validation and test examples", type=int, default=500)
parser.add_argument("num_models", help="number of modelts to train", type=int,  default=10)
parser.add_argument("max_epochs", help="max. number of epochs to train", type=int,  default=200)

# hparams string parsed to dict
parser.add_argument("hparams", help="architecture hyperparameters", type=str)

args = parser.parse_args()

"""
    Options
"""

# path to datasets
path_data = datapaths.path_data
path_root = os.getcwd()

# run name
run_type = args.model_type
run_basename = args.run_name

# number of training examples
num_train_samples = list([args.num_train])
num_train_samples = np.unique(num_train_samples)
train_sample_factor = 4

# set validation and test indices, so there is no overlap of data
num_val_and_test = args.num_val_test
validation_index_start = num_train_samples[-1]
validation_index_end = validation_index_start + num_val_and_test
test_index_start = validation_index_end
test_index_end = validation_index_end + num_val_and_test

# number of models to train for fixed architecture
num_models = args.num_models

# training parameters (lattice size, physical parameters)
# all lists should be of same length
mus = [1.000, 1.000, 1.500, 1.500]
worm_nums = [0, 1, 0, 1]
dims = [(8, 8), (8, 8), (8, 8), (8, 8)]
ms = [4.250, 4.250, 4.010, 4.010]
gs = [1.000, 1.000, 1.000, 1.000]

# maximum number of epochs to train for
max_epochs = args.max_epochs

# whether to print number of parameters before training
print_num_params = True


"""
    Definitions for each type of model
"""


# hparams parsed from args
def get_hparams(hparams_defaults):
    hparams = eval(args.hparams)
    hparams.update(vars(hparams_defaults))    
    hparams = argparse.Namespace(**hparams)

    # fix for flattening models
    if run_type == 'nonequivariant_fl':
        hparams.nt = dims[0][0]
        hparams.nx = dims[0][1]

    return hparams


if run_type == 'equivariant':
    # import model code
    from models import WormClassifier as Model

elif run_type == 'nonequivariant_gp':
    # import model code
    from models import WormClassifierNEQGP as Model

elif run_type == 'nonequivariant_fl':
    # import model code
    from models import WormClassifierNEQF as Model
    
else:
    print("Unknown run_type: {}".format(run_type))
    exit()

"""
    Load datasets
"""


def get_dataset_path(worm_num, dim, m, g, mu):
    nt, nx = dim
    # format: worm_num, nt, nx, m, g, mu
    dataset_string = "dataset-{:d}-{:d}-{:d}-{:.3f}-{:.3f}-{:.3f}.pt"
    dataset_path = os.path.join(path_data, dataset_string.format(worm_num, nt, nx, m, g, mu))
    if not os.path.exists(dataset_path):
        raise Exception("Path not found: " + dataset_path)
    return dataset_path


def concat_datasets(worm_nums, dims, ms, gs, mus, n_start=0, n_stop=-1):  
    # check if all lists have same length
    if not all([len(worm_nums) == len(lst) for lst in [dims, ms, gs, mus]]):
        print("List of parameters must all be same length!")
        raise
    
    paths = []
    for i in range(len(worm_nums)):
        paths.append(get_dataset_path(worm_nums[i], dims[i], ms[i], gs[i], mus[i]))
    
    datasets = []
    for p in paths:
        dataset = torch.load(p)

        if n_stop == -1:
            n_stop = len(dataset)            
        dataset = torch.utils.data.Subset(dataset, range(n_start, n_stop))
        
        datasets.append(dataset)
    
    combined_dataset = torch.utils.data.ConcatDataset(datasets)
        
    return combined_dataset


"""
    Training of single models
"""


class MetricsCallback(pl.Callback):
    def __init__(self):
        super().__init__()
        self.metrics = []
    
    def on_validation_end(self, trainer, pl_module):
        self.metrics.append(trainer.callback_metrics)


"""
    Training code
"""

# redirect all print statements to tqdm.write
# store builtin print
old_print = print


def new_print(*args, **kwargs):
    # if tqdm.tqdm.write raises error, use builtin print
    try:
        tqdm.tqdm.write(*args, **kwargs)
    except:
        old_print(*args, ** kwargs)


# globaly replace print with new_print
inspect.builtins.print = new_print

# create optuna path
optuna_path = os.path.join(path_root, 'optuna')
if not os.path.isdir(optuna_path):
    os.mkdir(optuna_path)

with tqdm(total=len(num_train_samples)) as pbar_samples:
    for current_num_train_samples in num_train_samples:
        # create training and validation subsets
        train_data = concat_datasets(worm_nums, dims, ms, gs, mus, 0, current_num_train_samples)
        val_data = concat_datasets(worm_nums, dims, ms, gs, mus, validation_index_start, validation_index_end)
        
        run_name = run_basename + '_{}_train_samples'.format(train_sample_factor * current_num_train_samples)
        model_path = os.path.join(optuna_path, run_name)
        pickle_path = os.path.join(path_root, 'optuna_pickles')
        
        """
            Default hparams
        """
        hparams_defaults = argparse.Namespace()
        
        # name
        hparams_defaults.name = run_name
        
        # workers for dataloaders
        hparams_defaults.num_workers = 0
        
        # optimizer
        hparams_defaults.lr = 1e-3
        hparams_defaults.batch_size = 100
        hparams_defaults.weight_decay = 0.0
        
        # lr scheduling
        hparams_defaults.lr_min = hparams_defaults.lr # this turns off the scheduler
        hparams_defaults.period_epochs = 10
        
        # results list
        results = []
        
        # training
        for model_no in range(args.num_models):
            print("##############")
            print("Starting model no. {:d}".format(model_no))
            
            # make a copy of default hparams
            hparams = argparse.Namespace(**vars(hparams_defaults))
            
            # suggest architecture
            hparams = get_hparams(hparams_defaults)

            # initialize model with hparams
            # we wrap this in the with block to turn off pytorch lightning info
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                model = Model(hparams, train_data, val_data, val_data)

            # print number of parameters
            if print_num_params:
                print("Number of trainable parameters: {}".format(model.count_parameters()))

            # tensorboard logger
            log_name = hparams.name + '_retrain_model_{:d}'.format(model_no)        
            tb = pl.loggers.TensorBoardLogger(save_dir=os.path.join(path_root, 'optuna_logs'), name=log_name)

            # checkpoints and metrics
            checkpoint_name = hparams.name + '_retrain_model_{:d}'.format(model_no)
            checkpoint_callback = pl.callbacks.ModelCheckpoint(os.path.join(model_path, checkpoint_name, '{epoch:03d}-{val_loss:.2E}'),
                                                               monitor='val_loss')
            metrics_callback = MetricsCallback()

            early_stop_callback = EarlyStopping(monitor='val_loss', min_delta=0.00,
                                                patience=max_epochs // 4, verbose=True, mode='min')
                                                
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                trainer = pl.Trainer(gpus=1, min_epochs=max_epochs // 4, max_epochs=max_epochs, weights_summary=None,
                                     check_val_every_n_epoch=1,
                                     progress_bar_refresh_rate=5, logger=tb, callbacks=[metrics_callback, early_stop_callback],
                                     distributed_backend=None,
                                     num_sanity_val_steps=0, checkpoint_callback=checkpoint_callback)
                trainer.fit(model)
            
            final_vloss = metrics_callback.metrics[-1]['val_loss']
            final_vacc = model.vacc
            best_vloss = checkpoint_callback.best_model_score.item()
            
            # store results
            res = {}
            res['name'] = log_name
            res['vloss'] = final_vloss
            res['best_vloss'] = best_vloss
            res['vacc'] = final_vacc
            res['model_checkpoint_path'] = os.path.join(model_path, checkpoint_name)
            res['hparams'] = hparams

            print("***************")
            print(log_name)
            print("vloss = {:.2E}, vacc = {:.3f}".format(final_vloss, final_vacc))
            
            results.append(res)
        
        sorted_results = sorted(results, key=lambda k: k['best_vloss']) 
        
        if not os.path.isdir(pickle_path):
            os.mkdir(pickle_path)
        
        results_filename = run_name + '_retrain.pickle'
            
        with open(os.path.join(pickle_path, results_filename), 'wb') as file:
            pickle.dump(sorted_results, file)
        
        pbar_samples.update(1)
        
print("All done!")
