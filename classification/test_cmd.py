"""
    This script simply takes an existing ensemble of models and tests them using the test datasets.
    The filenames of the ensembles are derived from the run_name and the number of training samples.

    Usage:
    test_cmd.py run_name model_type num_train num_test top_k
    - run_name: defines the output filenames of the run
    - model_type: either 'equivariant' (EQ), 'nonequivariant_gp' (ST) or 'nonequivariant_fl' (FLAT)
    - num_train: number of training samples  (multiplied by 4, 2 training points, 2 classes)
    - num_test: validation and test samples (multiplied by 4)
    - top_k: only consider the best top_k models
"""

"""
    Import packages
"""

import torch
import numpy as np
import os
import pickle
from itertools import product
import pytorch_lightning as pl
import argparse
import pickle
import re
import pandas
from tqdm import tqdm
import optuna
from operator import itemgetter
import itertools
import datapaths
import inspect

# suppress warning and logging
import warnings
import logging
import sys
warnings.filterwarnings('ignore')
logging.disable(sys.maxsize)

"""
    Command-line options
"""

parser = argparse.ArgumentParser()

parser.add_argument("run_name", help="name for optuna run", type=str)
parser.add_argument("model_type", help="type of model", type=str, choices=['equivariant', 'nonequivariant_gp', 'nonequivariant_fl'])
parser.add_argument("num_train", help="number of training examples (x4)", type=int, default=1000)
parser.add_argument("num_test", help="number of examples", type=int, default=500)
parser.add_argument("top_k", help="top k models to consider", type=int, default=500)

args = parser.parse_args()

"""
    Options
"""

# path to datasets
path_data = datapaths.path_data
path_root = os.getcwd()

# run name
run_type = args.model_type
run_basename = args.run_name

# training examples
num_train_samples = list([args.num_train])
num_train_samples = np.unique(num_train_samples)
train_sample_factor = 4

# test examples
num_test_examples = args.num_test

# top k
top_k = args.top_k

"""
    Definitions for each type of model
"""

if run_type == 'equivariant':
    # import model code
    from models import WormClassifier as Model
    
elif run_type == 'nonequivariant_gp':
    # import model code
    from models import WormClassifierNEQGP as Model
    
elif run_type == 'nonequivariant_fl':
    # import model code
    from models import WormClassifierNEQF as Model
    
else:
    print("Unknown run_type: {}".format(run_type))
    exit()

  
"""
    Load datasets
"""


def get_dataset_path(worm_num, dim, m, g, mu):
    nt, nx = dim
    # format: worm_num, nt, nx, m, g, mu
    dataset_string = "dataset-{:d}-{:d}-{:d}-{:.3f}-{:.3f}-{:.3f}.pt"
    dataset_path = os.path.join(path_data, dataset_string.format(worm_num, nt, nx, m, g, mu))
    if not os.path.exists(dataset_path):
        raise Exception("Path not found: " + dataset_path)
    return dataset_path


def concat_datasets(worm_nums, dims, ms, gs, mus, n_start=0, n_stop=-1):  
    # check if all lists have same length
    if not all([len(worm_nums) == len(lst) for lst in [dims, ms, gs, mus]]):
        print("List of parameters must all be same length!")
        raise
    
    paths = []
    for i in range(len(worm_nums)):
        paths.append(get_dataset_path(worm_nums[i], dims[i], ms[i], gs[i], mus[i]))
    
    datasets = []
    for p in paths:
        dataset = torch.load(p)

        if n_stop == -1:
            n_stop = len(dataset)            
        dataset = torch.utils.data.Subset(dataset, range(n_start, n_stop))
        
        datasets.append(dataset)
    
    combined_dataset = torch.utils.data.ConcatDataset(datasets)
        
    return combined_dataset


"""
    Load study and print top k models
"""

# quick overview using vloss
losses_from_pickle = []
results = None
for current_num_train_samples in num_train_samples:
    # get all paths
    run_name = run_basename + '_{}_train_samples'.format(train_sample_factor * current_num_train_samples)
    optuna_path = os.path.join(path_root, 'optuna')
    pickle_path = os.path.join(path_root, 'optuna_pickles')
    model_path = os.path.join(optuna_path, run_name)
    results_filename = run_name + '_retrain.pickle'
    
    with open(os.path.join(pickle_path, results_filename), 'rb') as f:
        results = pickle.load(f)

        
def key_func(x):
    return x['best_vloss']


results_sorted = sorted(results, key=key_func, reverse=False)

for t in results_sorted[:top_k]:
    model_checkpoint_path = t['model_checkpoint_path']
    best_vloss = t['best_vloss']
    vacc = t['vacc']
    print("Validation loss: {:.2E}, Validation acc: {}".format(best_vloss, vacc))
    
    reduced_hparams = {}
    dont_print = ['batch_size', 'num_workers', 'lr', 'lr_min', 'period_epochs', 'name']
    hparams = vars(t['hparams'])
    for attr in hparams:
        if attr in dont_print:
            pass
        else:
            reduced_hparams[attr] = hparams[attr]
    print("Hyperparameters:")
    print(reduced_hparams)

    
"""
    Load top k checkpoints
"""

# dummy datasets for model initialization
dummy_dataset = concat_datasets([0], [(8, 8)], [4.010], [1.000], [1.000], 0, 10)

models = []
for i, t in enumerate(results_sorted[:top_k]):
    model_checkpoint_path = t['model_checkpoint_path']
    checkpoint_files = os.listdir(model_checkpoint_path)
    
    checkpoint_file = None
    if len(checkpoint_files) > 1:
        print("Warning: model {} has more than one checkpoint.".format(i))
        for f in checkpoint_files:
            f_split = f.split("val_loss=")
            f_split = f_split[1].split(".ckpt")
            best_vloss = float(f_split[0])
            best_vloss_formated = float("{:.2E}".format(t['best_vloss']))
            if np.isclose(best_vloss, best_vloss_formated):
                print("{} fits the best_vloss metric.".format(f))
                checkpoint_file = f
        if checkpoint_file is None:
            print("No matching checkpoint found.")
            checkpoint_file = checkpoint_files[0]
    else:
        checkpoint_file = checkpoint_files[0]

    # load hparams from trial
    hparams = t['hparams']
    
    # intialize model with correct hparams and dummy datasets
    model = Model(hparams, dummy_dataset, dummy_dataset, dummy_dataset)
    
    # checkpoint data
    checkpoint = torch.load(os.path.join(model_checkpoint_path, checkpoint_file))
    
    # update model state_dict with checkpoint data
    model_dict = model.state_dict()
    pretrained_dict = {k: v for k, v in checkpoint['state_dict'].items() if k in model_dict}
    model_dict.update(pretrained_dict)
    model.load_state_dict(model_dict)
    model.eval()
    
    # add to models list
    models.append(model)
    
    
"""
    Test top k models on test data
"""

# redirect all print statements to tqdm.write
# store builtin print
old_print = print


def new_print(*args, **kwargs):
    # if tqdm.tqdm.write raises error, use builtin print
    try:
        tqdm.tqdm.write(*args, **kwargs)
    except:
        old_print(*args, ** kwargs)


# globaly replace print with new_print
inspect.builtins.print = new_print

# Load each dataset and test each model
results = []
for s in tqdm(os.listdir(path_data)):
    if re.match("dataset-[0-1]-", s) is not None:
        # extract physical parameters from filename
        split_str = os.path.splitext(s)[0].split("-")
        num = int(split_str[1])
        nt = int(split_str[2])
        nx = int(split_str[3])
        m = float(split_str[4])
        g = float(split_str[5])
        mu = float(split_str[6])
        
        # models with flatten work only on 8x8
        if run_type == 'nonequivariant_fl':
            if nx != 8:
                continue
        
        # load dataset
        dataset_path = os.path.join(path_data, s)
        dataset = torch.load(dataset_path)
        total_num = len(dataset)
        dataset = torch.utils.data.Subset(dataset, range(total_num - num_test_examples, total_num))
        
        # test best models on this data
        dataset_vloss = []
        dataset_vacc = []
        
        trainer = pl.Trainer(gpus=1, weights_summary=None, progress_bar_refresh_rate=0)
        
        for i, model in enumerate(models):
            
            # batch_size can be larger for testing
            model.hparams.batch_size = 100
            model.test_data = dataset
            trainer.test(model, test_dataloaders=torch.utils.data.DataLoader(dataset), verbose=False)
            
            # store results
            test_vloss = model.vloss.item()
            test_vacc = model.vacc.item()
            dataset_vloss.append(test_vloss)
            dataset_vacc.append(test_vacc)
        
        # save result to list
        entry = {'name': os.path.splitext(s)[0],
                 'path': os.path.join(path_data, s),
                 'num': num,
                 'nt': nt,
                 'nx': nx,
                 'm': m,
                 'g': g,
                 'mu': mu,
                 'vacc': dataset_vacc,
                 'vloss': dataset_vloss}
        results.append(entry)

df = pandas.DataFrame(results)
with open(os.path.join(pickle_path, run_basename + '_test_results.pickle'), 'wb') as file:
    pickle.dump(df, file)
